# Other

## Search

1. The `use_zoekt?` method should use state in order to decide if something is ready to be searched.
1. When we perform a search request, we should
  ```ruby
  scope = Search::Zoekt::EnabledNamespace.where(root_namespace_id: root_namespace_id, search: true)
  Search::Zoekt::Index.where(zoekt_enabled_namespace_id: scope.select(:id), state: [:ready, :reallocating]).pluck(:zoekt_node_id).sample
  ```

## Index

```ruby
scope = Search::Zoekt::EnabledNamespace.where(root_namespace_id: project.root_ancestor.id)
Search::Zoekt::Index.where(zoekt_enabled_namespace_id: scope.select(:id), state: [:initializing, :ready, :reallocating]).find_each do |index|
  repository = index.zoekt_repositories.find_or_create!(project_id: project.id)
  repository.tasks.create!(zoekt_node_id: index.zoekt_node_id, task_enum: :index)
end
```

## Delete

There's no really need for explicit deletions because Scheduling worker will mark the record 

```ruby
Search::Zoekt::Repository.where(project_id: project.id).update_all(state: :to_delete)
```

## Indexing a big namespace
A big namespace can't be indexed in a single node because repositories size of a big namespace iqquite big to fit into a singke node. The proposed solution is to create multiple indices for a big namespace. Each index will point to a different node. Here is the first proposal for the solution: https://gitlab.com/groups/gitlab-org/-/epics/14188#proposal

One `bigint` column `reserved_storage_bytes` will be added to `Search::Zoekt::Index`. The value of `reserved_storage_bytes` can be set equal to the some buffer multipler of the total root storage of the namespace during the `node_assignment`. If a namespace can not be fit into a single node, then we should split a big namespace and assign it to the multiple indices that belongs to the different nodes.

TODO:
Make a strategy to handle all the indices of a `node` whose sum of `reserved_storage_bytes` is greater than the `80%` of the node size or is less than the sum of all the assigned repositories size.



## Tasks API

1. Next task(s): `Search::Zoekt::Node.find_by(node_uuid).tasks.where(state: :pending).where('index_at < ?', Time.now).order(:index_at).limit(limit)`

1. We should return nothing if indexing is paused

#### Index

1. On success
  1. `task.zoekt_repository.update!(size_bytes: size_bytes, index_file_count: index_file_count, indexed_at: Time.now)`

#### Delete

1. On success
  1. `task.zoekt_repository.destroy!`

## Callback payloads

### Index

### Delete

### Merge