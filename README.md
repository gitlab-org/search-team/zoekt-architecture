# Zoekt architecture

## TOC
- [Schema design](./schema_design.md)
- [Workers](./workers.md)
- [Other](./other.md)

## Goals

### Minimal user intervention

Seeing how complicated it is for the Dedicated team to enable new customers I believe that as part of this architecture we should
make Zoekt integration click-and-forget type of feature.

For example, instead of manually adding namespaces, waiting until these are indexed, and enabling search, the scheduling worker should handle everything.

### Robust database-level integrity guarantees

I also believe that we should implement database level integrity gurantees that repositories will be removed from Zoekt
using `FK REFERENCES ... ON DELETE SET NULL`. We've been struggling with index integrity with Advanced Search for a while, so I think if we make Zoekt extremely robust from the ground up, that would be a major win.

An example:

1. A top level namespace is deleted
1. PostgreSQL automatically drops associated `zoekt_enabled_namespaces` record via `DELETE CASCADE`
1. That triggers `ON DELETE SET NULL` in `zoekt_indices.zoekt_enabled_namespace_id`
1. Scheduling worker marks these incides as `:orphaned`
1. Now the cleanup worker can go and search for `:orphaned` records and properly clean everything from Zoekt
1. The same goes for removing namespaces from Zoekt

Another example:

1. A project is deleted
1. PostgreSQL nullifies `zoekt_repositories.project_id` 
1. Scheduling worker marks these repositories as `:orphaned`
1. Now the cleanup worker finds `:orphaned` records and properly clean everything from Zoekt

### Way for `Search::Zoekt::Index` to understand if it's ready or not

When the scheduling worker assigns a namespace to `Search::Zoekt::Index`, we should be able to distinguish between a ready index and the
index that queued for indexing. Then we can avoid situations when customers search in an empty index and/or when admins are required to
manually enable/disable searching.