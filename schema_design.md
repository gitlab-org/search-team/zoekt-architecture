## Schema design

### Model: `Search::Zoekt::Node`

Table name: `zoekt_nodes`

| name                 | type        | comment                                                       |
| -------------------- | ----------- | ------------------------------------------------------------- |
|`id`|`bigint`|`PRIMARY KEY`|
|`uuid`|`uuid`|`NOT NULL`|
|`used_bytes`| `bigint`      | `NOT NULL` , `DEFAULT 0`|
|`total_bytes`|`bigint`|`NOT NULL`, `DEFAULT 0`|
|`last_seen_at`|`timestamp`|`NOT NULL`, `DEFAULT '1970-01-01 00:00:00+00'`|
|`index_base_url`|`text`| `NOT NULL`, `(char_length(index_base_url) <= 1024)`|
|`search_base_url`|`text`|`NOT NULL`, `(char_length(search_base_url) <= 1024)`|
|`metadata`|`jsonb`|`NOT NULL`, `DEFAULT '{}'`|
|`created_at`|`timestamp`|`NOT NULL`|
|`updated_at`|`timestamp`|`NOT NULL`|

Add validation: `root_namespace_id` can only point to a root namespace

* Indices
    - `btree (last_seen_at)`
    - `btree UNIQUE (uuid)`

### Model: `Search::Zoekt::EnabledNamespace`

Table name: `zoekt_enabled_namespaces`

| name                 | type        | comment                                                       |
| -------------------- | ----------- | ------------------------------------------------------------- |
| `id`                 | `bigint`    | `PRIMARY KEY`                                                 |
| `root_namespace_id`  | `bigint`    | `NOT NULL` , `FK REFERENCES namespaces(id) ON DELETE CASCADE` |
| `search`             | `bool`      | `NOT NULL` , `DEFAULT true` search enabled boolean flag       |
| `number_of_replicas` | `smallint`  | `NOT NULL`, `DEFAULT 1` (skip adding for now)                 |
| `created_at`         | `timestamp` | `NOT NULL`                                                    |
| `updated_at`         | `timestamp` | `NOT NULL`                                                    |

Add validation: `root_namespace_id` can only point to a root namespace

* Indices
    - `btree UNIQUE (root_namespace_id)`

### Model: `Search::Zoekt::Replica`

Table name: `zoekt_replicas`

| name                 | type        | comment                                                       |
| -------------------- | ----------- | ------------------------------------------------------------- |
|`id`|`bigint`|`PRIMARY KEY`|
|`zoekt_enabled_namespace_id`|`bigint`|`NOT NULL`, `FK REFERENCES zoekt_enabled_namespaces(id) ON DELETE CASCADE`|
|`namespace_id`|`bigint`|`NOT NULL`, `FK REFERENCES namespaces(id) ON DELETE CASCADE`|
|`state` |`smallint`|`NOT NULL`, `DEFAULT 0`|
|`created_at`|`timestamp`|`NOT NULL`|
|`updated_at`|`timestamp`|`NOT NULL`|

* Indices
    - `btree (zoekt_enabled_namespace_id)`
    - `btree (namespace_id, zoekt_enabled_namespace_id)`
    - `btree (state)`

* State Enum
    - 0: `pending`
    - 10: `ready`

### Model: `Search::Zoekt::Index`

Table name: `zoekt_indices`

| name                                  | type        | comment                                                         |
| ------------------------------------- | ----------- | --------------------------------------------------------------- |
| `id`                                  | `bigint`    | `PRIMARY KEY`                                                   |
| `zoekt_enabled_namespace_id`          | `bigint`    | `FK REFERENCES zoekt_enabled_namespaces(id) ON DELETE SET NULL` |
| `zoekt_node_id`                       | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_nodes(id) ON DELETE CASCADE`  |
| `state`                               | `smallint`  | `NOT NULL` , `DEFAULT 0`                                        |
| `zoekt_replica_id`                    | `bigint`    | `NOT NULL`, `FK REFERENCES zoekt_replicas(id) ON DELETE CASCADE`|
| `namespace_id`                        | `bigint`    | `NOT NULL`                                     |
| `created_at`                          | `timestamp` | `NOT NULL`                                                      |
| `updated_at`                          | `timestamp` | `NOT NULL`                                                      |
| `original_zoekt_enabled_namespace_id` | `bigint`    | `NOT NULL`                                                      |
| `metadata`                            | `jsonb`     | `NOT NULL` `DEFAULT '{}'::jsonb`                                |

Add uniqueness validation for `zoekt_enabled_namespace_id` and `zoekt_node_id`

* Indices
    - `btree (namespace_id, zoekt_enabled_namespace_id)`
    - `btree (zoekt_replica_id)`
    - `btree UNIQUE (state, id)`
    - `btree UNIQUE (zoekt_node_id, id)`
    - `btree UNIQUE (zoekt_enabled_namespace_id, zoekt_node_id)`

* State Enum
    - 0: `pending`
    - 1: `in_progress`   
    - 2: `initializing`
    - 10: `ready`
    - 20: `reallocating`
    - 30: `orphaned`
    - 40: `to_delete`
    - 50: `deleting`

Add a migration for existing records to set to :ready

### Model: `Search::Zoekt::Repository`

Table name: `zoekt_repositories`

| name                  | type        | comment                                                          |
| --------------------- | ----------- | ---------------------------------------------------------------- |
| `id`                  | `bigint`    | `PRIMARY KEY`                                                    |
| `zoekt_index_id`      | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_indices(id) ON DELETE CASCADE` |
| `project_id`          | `bigint`    | `FK REFERENCES projects(id) ON DELETE SET NULL`                  |
| `state`               | `smallint`  | `NOT NULL` , `DEFAULT 0`                                         |
| `size_bytes`          | `bigint`    |                                                                  |
| `index_file_count`    | `integer`   |                                                                  |
| `created_at`          | `timestamp` | `NOT NULL`                                                       |
| `updated_at`          | `timestamp` | `NOT NULL`                                                       |
| `indexed_at`          | `timestamp` |                                                                  |
| `project_identifier`  | `bigint`    | `NOT NULL`                                                       |

Add uniqueness validation for `zoekt_index_id` and `project_id`

* Indices
    - `btree UNIQUE (zoekt_index_id, project_id)`
    - `btree (project_id)`
    - `btree (state)`
* State Enum
    - 0: `pending`
    - 1: `initializing`
    - 10: `ready`
    - 30: `orphaned`
    - 40: `to_delete`
    - 50: `deleting`

### Model: `Search::Zoekt::Task`

Table name: `zoekt_tasks`

List partition strategy (partition id) sliding list strategy

https://docs.gitlab.com/ee/development/database/partitioning/list.html

https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125333

https://gitlab.com/gitlab-org/gitlab/-/merge_requests/125333/diffs#ba6507ad01d71b90ea490faf55250341451d0d7d

https://docs.gitlab.com/ee/development/cicd/cicd_tables.html

| name                   | type        | comment                                                               |
| ---------------------- | ----------- | --------------------------------------------------------------------- |
| `id`                   | `bigint`    | `PRIMARY KEY`                                                         |
| `zoekt_node_id`        | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_nodes(id) ON DELETE CASCADE`        |
| `zoekt_repository_id`  | `bigint`    | `NOT NULL` , `FK REFERENCES zoekt_repositories(id)`  `DELETE CASCADE` |
| `project_identifier`   | `bigint`    | `NOT NULL` Not a FK (sharding key)                                    |
| `state`                | `smallint`  | `NOT NULL` , `DEFAULT 0`                                              |
| `task_type`            | `smallint`  | `NOT NULL`                                                            |
| `retries_left`         | `smallint`  | `NOT NULL` , `DEFAULT 5`                                              |
| `serialized_arguments` | `text`      |                                                                       |
| `perform_at`           | `timestamp` | `NOT NULL`, `DEFAULT CURRENT_TIMESTAMP()`                             |
| `created_at`           | `timestamp` | `NOT NULL`                                                            |
| `updated_at`           | `timestamp` | `NOT NULL`                                                            |

* Validations
    - Validates that `project_identifier` matches `repository.project_id`
* DB Constraints
    - state has to be :failed if `retries_left == 0`
* Indices
    - `btree (zoekt_node_id, index_at, state)`
    - `btree (perform_at)`
    - `btree (zoekt_repository_id)`
* State Enum
    - 0: `pending`
    - 10: `done`
    - 255: `failed`
* Task Type Enum
    - 0: index
    - 50: delete
