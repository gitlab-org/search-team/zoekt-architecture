# Worker

## SchedulingWorker

- Worker class: `Search::Zoekt::SchedulingWorker`
- Service class: `Search::Zoekt::SchedulingService`

Scheduling service tasks runs in the following order:
- `auto_index_self_managed` creates the `Search::Zoekt::EnabledNamespace` records. `dot_com_rollout` is a temporary task which is also creating `Search::Zoekt::EnabledNamespace` records for the `dot_com` namespaces
- `node_assignment` will run over all the `Search::Zoekt::EnabledNamespace` records with missing indices and creates the `Search::Zoekt::Replica` and `Search::Zoekt::Index` records with `pending` state.
- `initial_indexing` task will pick up all the `pending` indices, starts the indexing and move the indices to `initializing`.
- `mark_indices_as_ready` will move all the `initializing` indices to `ready` whose all the `zoekt_repositories` are in the `ready` state.
- `reallocation` will recursively check if all the nodes have enough space. If any node space is full then it will delete the `Search::Zoekt::Index` records in that node. And ultimately, `node_assignment` task will create new indices in new node where enough space is available.
### Task: node_assignment

1. Verifies that `number_of_replicas` match `Search::Zoekt::Index` records
1. Uses namespace `namespace_statistics` to assign it to a node with the lowest disk utilization
1. Or remove records from the node with the highest disk utilization

### Task: initial_indexing

1. Iterates over `Search::Zoekt::Index.where(state: :pending)`
1. Calls `Search::Zoekt.index!` for all projects for that namespace

### Task: repository_indexing

1. Creates indexing tasks
   ```
   Search::Zoekt::Repository.where(state: :pending).find_each do |repository|
     repository.tasks.create!(zoekt_node_id: zoekt_node_id, task_enum: :index)
     repository.update!(state: :initializing)
   end
   ```

### Task: repository_deletion

1. Creates delete tasks
   ```
   Search::Zoekt::Repository.where(state: [:orphaned, :to_delete]).find_each do |repository|
     repository.tasks.create!(zoekt_node_id: zoekt_node_id, task_enum: :delete)
     repository.update!(state: :deleting)
   end
   ```

### Task: mark_indices_as_ready

1. Iterates over `Search::Zoekt::Index.where(state: :initializing)`
1. Checks that `index.repositores.where(state: [:pending, :initializing]).count == 0`
1. Updates the state `index.update!(state: :ready)`

### Task: mark_orphaned

1. Iterates over `Search::Zoekt::Index.where.not(state: :orphaned).where(zoekt_enabled_namespace_id: nil)`
1. Updates the state `index.update!(state: orphaned)`

---

1. Iterates over `Search::Zoekt::Repository.where.not(state: :orphaned).where(project_id: nil)`
1. Updates the state `index.update!(state: orphaned)`

### Task: cleanup

1. Looks for `zoekt_indices` with `state: [:orphaned, :to_delete]`
1. Updates associated repositories with `state: :to_delete`
1. Updates the `Search::Zoekt::Index` state to `:deleting`

---

1. Looks for `Search::Zoekt::Index.where(state: :deleting)`
1. Checks that `index.repositories.count == 0`
1. Destroys it

### Task: delete_offline_nodes

This task should delete all the offline nodes and attached indices to this node. 


### Task: auto_index_self_managed

This task is only executed on self-managed instances who have application setting `zoekt_auto_index_root_namespace` enabled 
1. Iterates over root Group Namespaces which don't have associated `Search::Zoekt::EnabledNamespace` record.
1. Creates the `Search::Zoekt::EnabledNamespace` records for each root groups

### Task: reallocation

1. Iterate over `Search::Zoekt::Index.where(state: :reallocating)`
1. Verify if target index is ready
1. If it's ready update the original record with `state: :to_delete`

---

1. Iterate over offline nodes (more than an hour)
1. Set associated `Search::Zoekt::Index` records to `:to_delete`

---

1. Skip the execution if there are existing `Search::Zoekt::Index.where(state: :reallocating)` records
1. Check if any Zoekt nodes are over the watermark limit (80%). Pick the node with the maximum watermark
1. Collect indices `state: :ready` starting from the smallest ones that if moved would bring it closer to the watermark limit.
1. Iterate over the records
1. Generate a plan for reallocation to a temporary struct/hash
1. Assign it to a node with lowest watermark limit. Ensure that it won't bring the nodes over the limit
1. Keep track of expected watermark limit after applying planned assignments
1. Start a DB transaction
1. Create new `Search::Zoekt::Index` records with `state: :pending`
1. Mark old index records as `state: :reallocating, metadata: {target_zoekt_index_id: <zoekt_index_id>}`

Note: We'll need to update https://gitlab.com/gitlab-org/gitlab/-/blob/e2ef1413b7a95f0073a373a693d3d5b39fe5dabd/ee/app/models/search/zoekt.rb#L19 so that
we can search indices with scope: %i[ready reallocating]

## Callback worker

Worker class: `Search::Zoekt::CallbackWorker`

1. Always updates `zoekt_tasks` on success/failure
1. For success, it sets `state` to `:done`
1. For failure, it decreases `retries_left` by 1 and logs the error
1. If there are no retries left, it does `update!(retries_left: 0, state: :failed)`